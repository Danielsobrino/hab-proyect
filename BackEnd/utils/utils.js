// 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
const fs = require('fs').promises;
const multer = require('multer');
const uuid = require('uuid');
const path = require('path');
const sharp = require('sharp');
require('dotenv').config();

//ruta de la guardado de los archivos
const imagenUploadPath = path.join(__dirname, process.env.TARGET_FOLDER)

//subida de videos
const videoName = `${uuid.v4()}.mp4`;
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, imagenUploadPath);
	},
	filename: (req, file, cb) => {
        cb(null, videoName);
	},
});
const uploadVideoFile = multer({ storage });


//subida de imagenes

async function processAndSaveImages(uploadedImage) {
    await fs.mkdir(imagenUploadPath, { recursive: true });
    const image = sharp(uploadedImage.data);
    const imageFileName = `${uuid.v4()}.jpg`;
    await image.toFile(path.join(imagenUploadPath, imageFileName));
    return imageFileName;
}


//envio de emails
const sendgrid = require("@sendgrid/mail");

// email confirmacion
const sendconfirmationMail = async (email, link) => {
    sendgrid.setApiKey(process.env.SEND_EMAIL);

    const message = {
        to: email,
        from: 'danielsobrino08@gmail.com',
        subject: 'validate your account',
        text: `la direccion de verificacion es: ${link}`,
        html: `<div>
    <h1> valida tu registro en sonar</h1>
    <p> para validar si te registraste, accede aqui</p>
    ${link}
</div>
`
        ,

    };
    await sendgrid.send(message)
}


//email de recuperacion
const sendRecoveryMail = async (email, link, recoveryCode) => {
    sendgrid.setApiKey(process.env.SEND_EMAIL);

    const message = {
        to: email,
        from: 'danielsobrino08@gmail.com',
        subject: 'validate your account',
        text: `el codigo que debes introducir es: ${recoveryCode}
                en el siguiente enlace. ${link}`,
        html: `<div>
    <h1> olvidaste la contraseña</h1>
    <p> para introducir tu code pulsa aqui</p>
    ${link}
</div>
`
        ,

    };
    await sendgrid.send(message)
}


//email de contrato
const sendContratMail = async (email, link, movil) => {
    sendgrid.setApiKey(process.env.SEND_EMAIL);

    const message = {
        to: email,
        from: 'danielsobrino08@gmail.com',
        subject: 'opcion de contratacion',
        text: `Me complace comunicarle que el club en el que trabajo estaria interesado en la contratacion de su hij@`,
        html: `<div>
    <h1> CONTRATACION</h1>
    <p> Nos gustaria que se pusiese en contacto con nosotros para la realizacion de una pruebas para la posible contratacion</p>
    <p> ${movil} </p> 
    <p> puede ver la informacion de la solicitud asi como querer o no seguir en nuestro proceso en la siguiente pagina </p>
    <p> ${link} </p>  
</div>
`
        ,

    };
    await sendgrid.send(message)
}











module.exports = {
    sendconfirmationMail,
    sendContratMail,
    sendRecoveryMail,
    processAndSaveImages,
    uploadVideoFile
}



