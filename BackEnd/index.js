require('dotenv').config();


const bodyParser = require('body-parser');
const express = require('express');
var cors = require('cors')
const morgan = require('morgan')



const {createFamily, deleteFamily, updateFamily,selectFamily, loginFamily, forgetPasswordf,resetPasswordf,validate} = require('./controllers/familia');
const {createPlayer, deletePlayer, onePlayer, updatePlayer, filterPlayer, listPlayer} = require('./controllers/jugadores');
const {createOjeadores,forgetPasswordo,resetPasswordo, updateOjeadores,LoginOjeadores, deleteOjeador,selectOjeadores,validateOjeador} = require('./controllers/ojeadores');
const {createVideos,deleteVideo,selectVideo, selectVideolist, uploadVideoFile} = require('./controllers/videos')
const { createContrato, selectContratos, confirmationContrat,selectOneContrat} = require('./controllers/contratos');

const { isLoginFamily} = require('./middlewares/authFamilia');
const {isLoginOjeador} =require('./middlewares/authOjeadores');
const { isSameFamilyfor } = require('./middlewares/authjugadores');
const {familyForVideo} = require('./middlewares/authVideos');
const {isLogin} = require('./middlewares/authgeneral');
const {familyForContrat} =require('./middlewares/authContratos')
const fileUpload = require('express-fileupload');

const app = express();

app.use(morgan('dev'));

//importante para la subida de archivos
app.use(fileUpload());
app.use(express.static('static'));
app.use(express.static('utils/images'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors())

//subida video
app.post('/sonar/player/videos/:id_jugador',isLoginFamily,isSameFamilyfor, createVideos)

//FILTRADO ANONIMO
app.get('/sonar/player', filterPlayer) 


// FAMILIAS
app.post('/sonar/family', createFamily)
app.get('/sonar/family/validate/:code', validate)

app.post('/sonar/family/login', loginFamily)
app.get('/sonar/family', isLoginFamily, selectFamily)
app.put('/sonar/family/update', isLoginFamily, updateFamily)
app.delete('/sonar/family/delete',isLoginFamily, deleteFamily)


app.put('/sonar/family/forget', forgetPasswordf)
app.put('/sonar/family/reset', resetPasswordf)

// JUGADOR
app.post('/sonar/player', isLoginFamily, createPlayer)
app.delete('/sonar/player/delete/:id_jugador', isLoginFamily, isSameFamilyfor, deletePlayer )
app.get('/sonar/player/:id_jugador', isLogin, onePlayer)
app.get('/sonar/players', isLoginFamily, listPlayer)
app.put('/sonar/player/update/:id_jugador', isLoginFamily,isSameFamilyfor, updatePlayer )


// OJEADORES
app.post('/sonar/ojeadores', createOjeadores)
app.get('/sonar/ojeador/validate/:code', validateOjeador)

app.post('/sonar/ojeadores/login',LoginOjeadores) 
app.get('/sonar/ojeador', isLoginOjeador, selectOjeadores)
app.put('/sonar/ojeadores',isLoginOjeador, updateOjeadores)
app.delete('/sonar/ojeadores',isLoginOjeador, deleteOjeador)

app.put('/sonar/ojeadores/forget', forgetPasswordo)
app.put('/sonar/ojeadores/reset', resetPasswordo)


// VIDEOS
app.delete('/sonar/player/videos/:idvideo',isLoginFamily, familyForVideo, deleteVideo)

app.get('/sonar/player/videos/select/:idvideo',isLogin, selectVideo)
app.get('/sonar/player/videos/selectall/:idjugador',isLogin, selectVideolist)

// SOLICITUD CONTRATOS
app.post('/sonar/contratos/:idjugador',isLoginOjeador, createContrato)
app.post('/sonar/contratos/confirmation/:code',isLoginFamily, familyForContrat ,confirmationContrat)

app.get('/sonar/contratos/oneContrat/:idcontrato', isLogin, selectOneContrat)
app.get('/sonar/contratos/select/:idjugador',isLogin, selectContratos)



app.use((error, req, res, next) => {
	res.send(`Este es tu error: ${error.message}`);
});

app.use((req, res) => res.status(404).send("Página no encontrada"));




app.listen(process.env.PORT)