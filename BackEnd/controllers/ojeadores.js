require("dotenv").config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const randomstring = require("randomstring");
const { getConnection } = require("../db/db");
const { processAndSaveImages } = require('../utils/utils')

const utils = require('../utils/utils')

async function createOjeadores(req, res, next) {

    let connection;
    try {
        connection = await getConnection();
        const validationCode = randomstring.generate(20);
        const {
            nombre,
            apellido_1,
            apellido_2,
            descripcion,
            provincia,
            club,
            email,
            contrasena,
            telefono } = req.body

        const passwordBcrypt = await bcrypt.hash(contrasena, 10);
        
        let picture;

        if (req.files.avatar) {
            try {
                picture = await processAndSaveImages(req.files.avatar)
            } catch (e) {
                console.log(e)
            }
        }

        await connection.query(`
        INSERT INTO ojeadores(
            fecha_creacion,
            fecha_modificacion,
            nombre, 
            apellido_1, 
            apellido_2,
            descripcion,
            provincia,
            club,
            email,
            avatar,
            contrasena,
            telefono,
            validationCode)
        VALUES (UTC_TIMESTAMP,UTC_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?)`,
            [nombre,apellido_1,apellido_2,descripcion,provincia,club,email,picture,passwordBcrypt,telefono,validationCode])
            
        console.log('OJEADOR CREADO CON EXITO')

        const link = `http://localhost:9999/sonar/ojeador/validate/${validationCode}`
        utils.sendconfirmationMail(email, link)


        res.send('REVISA CORREO PARA LA VERIFICACION')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



const validateOjeador = async (req, res, next) => {
    const { code } = req.params;

    let connection;
    try {
        connection = await getConnection();
        const [result] = await connection.query(`
           SELECT * FROM ojeadores WHERE validationCode = ? `, [code])

        if (result) {
            await connection.query(`
            UPDATE ojeadores SET active = true, validationCode = '' WHERE validationCode = ? `, [code])
            res.send()

        } else {
            throw new Error('validation-error');
        }
        
        console.log('validacion correcta')

    } catch (e) {
        res.status(401).send('user no valido')
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }

}

async function LoginOjeadores(req, res, next) {
    let connection;
    try {
        connection = await getConnection();

        const { email, contrasena } = req.body

        const [resultado] = await connection.query(`
            SELECT * FROM ojeadores WHERE email = ? AND active = true
            `, [email])

        const [user] = resultado

        if (!user) {
            res.status(401).send()
            return
        }

        // comprobamos que la contraseña coincide
        const contrasenaValid = await bcrypt.compare(contrasena, user.contrasena);

        if (!contrasenaValid) {
            res.status(401).send()
            return
        }

        // informacion que queremos ue lleve el token
        const tokenInfo = {
            id: user.id,
            email: user.email,
            user: user.telefono,
            role: 'ojeador',
            avatar: user.avatar

        }

        // creamos token con la "llave" secret
        const token = jwt.sign(tokenInfo, process.env.SECRET, {
            expiresIn: '1d'
        })

        res.send({token})
        console.log('SESION INICIADA COMO OJEADOR')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


async function deleteOjeador(req, res, next) {

    const { id } = req.auth;
    let connection;
    try {
        connection = await getConnection();
        await connection.query(`
           DELETE FROM ojeadores WHERE id = ? `, [id])
        res.send()
        console.log('OJEADOR BORRADO')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


async function updateOjeadores(req, res, next) {
    const { id } = req.auth;
    let connection;
    try {
        connection = await getConnection();

        const {
            nombre,
            apellido_1,
            apellido_2,
            descripcion,
            provincia,
            club,
            email,
            contrasena,
            telefono } = req.body

        const passwordBcrypt2 = await bcrypt.hash(contrasena, 10);

        let picture;

        if (req.files.avatar) {
            try {
                picture = await processAndSaveImages(req.files.avatar)
            } catch (e) {
                console.log(e)
            }
        }

        await connection.query(`
        UPDATE ojeadores
        SET
            fecha_creacion = UTC_TIMESTAMP,
            nombre = ?,
            apellido_1 = ?, 
            apellido_2 = ?,
            descripcion = ?,
            provincia = ?,
            club = ?,
            email = ?,
            avatar = ?,
            contrasena = ?,
            telefono = ?
        WHERE id = ? `
            ,
            [nombre,
                apellido_1,
                apellido_2,
                descripcion,
                provincia,
                club,
                email,
                picture,
                passwordBcrypt2,
                telefono,
                id])
        res.send()

        console.log('OJEADOR MODIFICADO CORRECTAMENTE')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}




async function selectOjeadores(req, res, next) {

    const { id } = req.auth;
    let connection;
    try {
        connection = await getConnection();
        const [ojeador] = await connection.query(`
           SELECT * FROM ojeadores WHERE id = ? `, [id])
        res.send({ojeador})
        console.log('INFORMACION DEL OJEADOR')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



//olvide contraseña

async function forgetPasswordo(req, res, next) {
    let connection;
    try {
        connection = await getConnection();

        const { email } = req.body
        const [id_ojeador] = await connection.query(`
         SELECT id FROM ojeadores WHERE email = ? `,
            [email]);
        
        const idojeador = id_ojeador[0].id
        const recoveryCode = randomstring.generate(8);


        await connection.query(`
                UPDATE ojeadores
                 SET
                 fecha_modificacion = UTC_TIMESTAMP,
                 recoveryCode = ?
                WHERE id = ?
                
                     `,
            [
                recoveryCode,
                idojeador
            ])

        //enviamos correo para hacer el posterior reset de contraseña
        //meter la ruta de FRONTEND
        const link = `http://localhost:9999/sonar/family/reset`
        utils.sendRecoveryMail(email, link, recoveryCode)

        res.send('modificado')
        console.log('enviado codigo a ojeador')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}




//RESETEAR CONTRASEÑA

async function resetPasswordo(req, res, next) {
    let connection;
    try {
        connection = await getConnection();

        const { recoveryCode,
            newpassword } = req.body


            const passwordBcrypt = await bcrypt.hash(newpassword, 10);
        //modificamos la contraseña de usuario que tiene ese recovery code y lo ponemos a 0
        await connection.query(`
                UPDATE ojeadores
                 SET
                 contrasena = ?,
                 recoveryCode = '' 
                WHERE recoverycode = ?
                     `,
            [
                passwordBcrypt,
                recoveryCode
            ])
        res.send()
        console.log('contrasena cambiada, ojeador')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



module.exports = {
    createOjeadores,
    deleteOjeador,
    forgetPasswordo,
    resetPasswordo,
    LoginOjeadores,
    selectOjeadores,
    updateOjeadores,
    validateOjeador
}
