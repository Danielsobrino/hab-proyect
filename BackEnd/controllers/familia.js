require("dotenv").config();
const randomstring = require("randomstring");
const { getConnection } = require("../db/db");
const { processAndSaveImages } = require('../utils/utils')

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


const utils = require('../utils/utils')


//CREAMOS LA FAMILIA

async function createFamily(req, res, next) {

    let connection;
    try {
        connection = await getConnection();
        const validationCode = randomstring.generate(20);

        const {
            nombre,
            apellido_1,
            apellido_2,
            provincia,
            ciudad,
            email,
            contrasena,
            telefono } = req.body

        // encriptamos la contrasena
        const passwordBcrypt = await bcrypt.hash(contrasena, 10);

        //subida imagen 
        let picture;

        if (req.files.avatar) {
            try {
                picture = await processAndSaveImages(req.files.avatar)
            } catch (e) {
                console.log(e)
            }
        }

        await connection.query(`
            INSERT INTO familia(
                fecha_creacion,
                fecha_modificacion,
                nombre,
                apellido_1,
                apellido_2,
                provincia,
                ciudad,
                email,
                avatar,
                contrasena,
                telefono,
                validationcode)
            VALUES (UTC_TIMESTAMP,UTC_TIMESTAMP,?,?,?,?,?,?,?,?,?,?)`,
            [nombre, apellido_1, apellido_2, provincia, ciudad, email, picture, passwordBcrypt, telefono, validationCode])

        console.log('FAMILIA CREADA CON EXITO')


        //enviamos correo validacion
        const link = `http://localhost:9999/sonar/family/validate/${validationCode}`
        utils.sendconfirmationMail(email, link)


        res.send('REVISAR CORREO PARA LA VERIFICACION.')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


const validate = async (req, res, next) => {
    const { code } = req.params;
    let connection;
    try {
        connection = await getConnection();
        const [result] = await connection.query(`
           SELECT * FROM familia WHERE validationCode = ? `, [code])
        if (result) {
            await connection.query(`
            UPDATE familia SET active = true, validationCode = '' WHERE validationCode = ? `, [code])
            res.send()
        } else {
            throw new Error('validation-error');
        } 
        console.log('validacion correcta')

    } catch (e) {
        res.status(401).send('user no valido')
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


//NOS LOGUEAMOS COMO FAMILIA
async function loginFamily(req, res, next) {
    let connection;
    try {

        connection = await getConnection();

        const { email, contrasena } = req.body

        const [resultado] = await connection.query(`
            SELECT * FROM familia WHERE email = ? AND active = true
            `, [email])

        const [user] = resultado

        if (!user) {
            res.status(401).send()
            return
        }

        // comprobamos que la contraseña coincide
        const contrasenaValid = await bcrypt.compare(contrasena, user.contrasena);

        if (!contrasenaValid) {
            res.status(401).send()
            return
        }

        // informacion que queremos ue lleve el token
        const tokenInfo = {
            id: user.id,
            email: user.email,
            user: user.telefono,
            role: 'familia',
            avatar: user.avatar
        }

        // creamos token con la "llave" secret
        const token = jwt.sign(tokenInfo, process.env.SECRET, {
            expiresIn: '1d'
        })
        res.send({ token })
        console.log('SESION INICIADA COMO FAMILIA')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


//OPCION BORRADO(TOTAL) FAMILIA
async function deleteFamily(req, res, next) {

    const { id } = req.auth;
    let connection;
    try {
        connection = await getConnection();
        await connection.query(`
           DELETE FROM familia WHERE id = ? `, [id])
        res.send()
        console.log('FAMILIA BORRADA')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



//MODIFICACION DEL PERFIL 
async function updateFamily(req, res, next) {
    const { id } = req.auth;
    let connection;
    try {
        connection = await getConnection();

        const {
            nombre,
            apellido_1,
            apellido_2,
            provincia,
            ciudad,
            email,
            contrasena,
            telefono } = req.body

        const passwordBcrypt2 = await bcrypt.hash(contrasena, 10);


        let picture;

        if (req.files.avatar) {
            try {
                picture = await processAndSaveImages(req.files.avatar)
            } catch (e) {
                console.log(e)
            }
        }

        await connection.query(`
                UPDATE familia
                 SET
                 fecha_modificacion = UTC_TIMESTAMP,
                 nombre = ? ,
                 apellido_1 = ? ,
                 apellido_2 = ? ,
                 provincia = ? ,
                 ciudad = ? ,
                 email = ? ,
                 avatar = ? ,
                 contrasena = ? ,
                 telefono  = ? 
                WHERE id = ?
                
                     `,
            [nombre,
                apellido_1,
                apellido_2,
                provincia,
                ciudad,
                email,
                picture,
                passwordBcrypt2,
                telefono,
                id
            ])
        res.send()

        console.log('FAMILIA MODIFICADA CORRECTAMENTE')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


//SELECCION DE UN PERFIL EN CONCRETO.
async function selectFamily(req, res, next) {

    const { id } = req.auth;
    let connection;
    try {
        connection = await getConnection();
        const [familia] = await connection.query(`
           SELECT * FROM familia WHERE id = ? `, [id])
        res.send({ familia })
        console.log('INFORMACION DE LA FAMILIA')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}




//olvide contraseña

async function forgetPasswordf(req, res, next) {
    let connection;
    try {
        connection = await getConnection();

        const { email } = req.body
        const [id_familia] = await connection.query(`
         SELECT id FROM FAMILIA WHERE email = ? `,
            [email]);
        const idfamilia = id_familia[0].id
        const recoveryCode = randomstring.generate(8);


        await connection.query(`
                UPDATE familia
                 SET
                 fecha_modificacion = UTC_TIMESTAMP,
                 recoveryCode = ?, 
                WHERE id = ?
                
                     `,
            [
                recoveryCode,
                idfamilia
            ])

        //enviamos correo para hacer el posterior reset de contraseña
        //meter la ruta de FRONTEND
        const link = `http://localhost:9999/sonar/family/reset`
        utils.sendRecoveryMail(email, link, recoveryCode)


        console.log('envidado codigo a familia')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}




//RESETEAR CONTRASEÑA

async function resetPasswordf(req, res, next) {
    let connection;
    try {
        connection = await getConnection();

        const { recoveryCode,
            newpassword } = req.body

            const passwordBcrypt = await bcrypt.hash(newpassword, 10);
        //modificamos la contraseña de usuario que tiene ese recovery code y lo ponemos a 0
        await connection.query(`
                UPDATE familia
                 SET
                 contrasena = ?,
                 recoveryCode = ''  
                WHERE recoverycode = ?
                     `,
            [
                passwordBcrypt,
                recoveryCode
            ])
        res.send()
        console.log('contrasena cambiada, familia')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}




module.exports = {
    createFamily,
    deleteFamily,
    forgetPasswordf,
    loginFamily,
    resetPasswordf,
    selectFamily,
    updateFamily,
    validate

}