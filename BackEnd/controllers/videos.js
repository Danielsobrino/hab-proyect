require("dotenv").config();
const { processAndSaveImages } = require('../utils/utils')
const { getConnection } = require("../db/db");



// const multer = require('multer');



// const imagenUploadPath = path.join(__dirname, process.env.TARGET_FOLDER)
// const videoName = `${uuid.v4()}.mp4`;
// const storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         cb(null, imagenUploadPath);
//     },
//     filename: (req, file, cb) => {
//         cb(null, videoName);
//     },
// });
// const uploadVideoFile = multer({ storage });
//video = await uploadVideoFile(req.files.url_video)
async function createVideos(req, res, next) {
    let connection;
    try {
        connection = await getConnection();
        const idfamilia = req.auth.id
        const { id_jugador } = req.params
        const {
            titulo,
            mensaje
        } = req.body
        if (req.files) {{
            try {
                picture = await processAndSaveImages(req.files.url_video)
            } catch (e) {
                console.log(e)
            }
        }
            await connection.query(`
            INSERT INTO videos(
                fecha_creacion,
                titulo,
                mensaje,
                url_video,
                id_jugador,
                id_familia )
                VALUES (UTC_TIMESTAMP,?,?,?,?,?)`,
                [
                    titulo,
                    mensaje,
                    picture,
                    id_jugador,
                    idfamilia])
            res.send()
            console.log('video subido')

        } else {
            console.log('error')
        }

    } catch (e) {
        console.log(e, 'error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


async function deleteVideo(req, res, next) {
    const { idvideo } = req.params;
    let connection;
    try {
        connection = await getConnection();
        await connection.query(`
        DELETE FROM videos WHERE id = ?` ,
            [idvideo])
        res.send()
        console.log('video borrado')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}

async function selectVideo(req, res, next) {
    const { idvideo } = req.params;
    let connection;
    try {
        connection = await getConnection();
        const [video] = await connection.query(`
        SELECT * FROM videos WHERE id = ?` ,
            [idvideo])
        res.send(video[0])
        console.log('INFORMACION DEL VIDEO')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



async function selectVideolist(req, res, next) {
    const { idjugador } = req.params;
    let connection;
    try {
        connection = await getConnection();
        const [video] = await connection.query(`
        SELECT fecha_creacion, titulo, mensaje, url_video FROM videos WHERE id_jugador = ?`, [idjugador])
        res.send(video)
        console.log('lista')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



module.exports = {
    createVideos,
    deleteVideo,
    selectVideo,
    selectVideolist,
    
}