require("dotenv").config();
const { processAndSaveImages } = require('../utils/utils')

const { getConnection } = require("../db/db");


async function createPlayer(req, res, next) {
    let connection;
    try {
        connection = await getConnection();
        const id_familia = req.auth.id
        const emailfamilia = req.auth.email


        const {
            dni,
            nombre,
            apellido_1,
            apellido_2,
            descripcion,
            edad,
            sexo,
            provincia,
            categoria,
            club,
            posicion_principal,
            pierna,
            altura,
            peso
        } = req.body

        let picture;

        if (req.files.avatar) {
            try {
                picture = await processAndSaveImages(req.files.avatar)
            } catch (e) {
                console.log(e)
            }
        }

        await connection.query(`
        INSERT INTO jugadores(
            dni,
            email,
            nombre , 
            apellido_1, 
            apellido_2,
            descripcion,
            edad,
            sexo,
            provincia,
            categoria,
            club,
            posicion_principal,
            pierna,
            altura,
            peso,
            avatar,
            id_familia)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
            [dni,
                emailfamilia,
                nombre,
                apellido_1,
                apellido_2,
                descripcion,
                edad,
                sexo,
                provincia,
                categoria,
                club,
                posicion_principal,
                pierna,
                altura,
                peso,
                picture,
                id_familia])


        res.send('jugador creado con exito')
        console.log('jugador creado')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


async function deletePlayer(req, res, next) {

    const { id_jugador } = req.params;
    let connection;
    try {
        connection = await getConnection();
        await connection.query(`
           DELETE FROM jugadores WHERE id = ? `, [id_jugador])
        res.send()
        console.log('JUGADOR BORRADO')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



async function onePlayer(req, res, next) {
    const { id_jugador } = req.params;
    let connection;
    try {
        connection = await getConnection();
        const [player] = await connection.query(`
                   SELECT * FROM jugadores WHERE id=?`, [id_jugador])

        res.send(player[0])
        console.log('perfil obtenido')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}

async function listPlayer(req, res, next) {
    const id_familia = req.auth.id;
    console.log(id_familia)
    let connection;
    try {
        connection = await getConnection();
        const [player] = await connection.query(`
                   SELECT * FROM jugadores WHERE id_familia = ?`, [id_familia])

        res.send({player})
        console.log('perfil obtenido')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



async function updatePlayer(req, res, next) {
    const { id_jugador } = req.params;
    let connection;
    try {
        connection = await getConnection();
        const {
            dni,
            nombre,
            apellido_1,
            apellido_2,
            descripcion,
            edad,
            sexo,
            provincia,
            categoria,
            club,
            posicion_principal,
            pierna,
            altura,
            peso
        } = req.body

        let picture;

        if (req.files.avatar) {
            try {
                picture = await processAndSaveImages(req.files.avatar)
            } catch (e) {
                console.log(e)
            }
        }

        await connection.query(`
                    UPDATE jugadores
                     SET
                        nombre = ?, 
                        apellido_1 = ?,
                        apellido_2 = ?,
                        descripcion = ?,
                        edad = ?,
                        sexo = ?,
                        provincia = ?,
                        categoria = ?,
                        club = ?,
                        posicion_principal = ?,
                        pierna = ?,
                        altura = ?,
                        peso = ?,    
                        avatar = ? 
                    WHERE id = ?
                         `,
            [
                nombre,
                apellido_1,
                apellido_2,
                descripcion,
                edad,
                sexo,
                provincia,
                categoria,
                club,
                posicion_principal,
                pierna,
                altura,
                peso,
                picture,
                id_jugador
            ])
        res.send()

        console.log('perfil modificado')


    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}



async function filterPlayer(req, res, next) {

    let connection;
    try {
        connection = await getConnection();

        const { fecha_nacimiento, posicion_principal, club, pierna } = req.query;

        let query = "SELECT id, nombre, categoria, club, posicion_principal FROM jugadores";
        const params = [];


        if (fecha_nacimiento || posicion_principal || club || pierna) {
            const conditions = [];
            if (fecha_nacimiento) {
                conditions.push(`fecha_nacimiento = ? `);
                params.push(`${fecha_nacimiento}`);
            }
            if (posicion_principal) {
                conditions.push(`posicion_principal = ? `);
                params.push(`${posicion_principal}`);
            }
            if (club) {
                conditions.push(`club = ? `);
                params.push(`${club}`);
            }
            if (pierna) {
                conditions.push(`pierna = ? `);
                params.push(`${pierna}`);
            }

            query = `${query} WHERE ${conditions.join(
                ' AND '
            )}`;

            const [result] = await connection.query(query, params)


            res.send({
                data: result
            }
            )
        }
    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


module.exports = {
    createPlayer,
    deletePlayer,
    onePlayer,
    updatePlayer,
    filterPlayer,
    listPlayer
}