const { getConnection } = require("../db/db");
const randomstring = require("randomstring");
const utils = require('../utils/utils')


async function createContrato(req, res, next) {
    const id = req.auth.id
    const { idjugador } = req.params
    const code = randomstring.generate(15);
    let connection;
    try {
        connection = await getConnection();
        const {
            titulo,
            mensaje,
            email
        } = req.body
       
        await connection.query(`
        INSERT INTO jugador_ojeador(
            titulo,
            mensaje,
            email,
            code,
            id_jugador,
            id_ojeador )
        VALUES (?,?,?,?,?,?)`,
            [titulo,
                mensaje,
                email,
                code,
                idjugador,
                id])

        const [telf] = await connection.query(`
        SELECT telefono FROM ojeadores WHERE id = ?`,[id])
            
        const movil = telf[0].telefono

        //enviamos correo validacion
        const link = `http://localhost:3000/contrato/confirmacion/${code}` //ruta FrontEnd

        //enviamos el correo con:
                //link para ver el correo.
                //telefono para las pruebas

        utils.sendContratMail(email, link, movil)

        res.send('revise su correo')

    } catch (e) {
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


const confirmationContrat = async (req, res, next) => {
    const codigo = req.params;
   const code = codigo.code
    const {estado} = req.body;
    let connection;
    try {
        connection = await getConnection();
        const [result] = await connection.query(`
           SELECT * FROM jugador_ojeador WHERE code = ? `, [code])
        if (result) {
            await connection.query(`
            UPDATE jugador_ojeador SET estado = ? , code = '' WHERE code = ? `, [estado, code])
            res.send()
        } else {
            throw new Error('validation-error');
        } 
        console.log('validacion correcta')
        
    } catch (e) {
        res.status(401).send('user no valido')
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}


const selectContratos = async (req, res, next) => {
    const { idjugador } = req.params;

    let connection;
    try {
        connection = await getConnection();
        const [result] = await connection.query(`
           SELECT titulo, estado FROM jugador_ojeador WHERE id_jugador = ? `, [idjugador])
        res.send(result)
        console.log('lista contratos')
        
    } catch (e) {
        res.status(401).send('user no valido')
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}

const selectOneContrat = async (req, res, next) => {
    const { idcontrato } = req.params;

    let connection;
    try {
        connection = await getConnection();
        const [result] = await connection.query(`
           SELECT * FROM jugador_ojeador WHERE id = ? `, [idcontrato])
        res.send(result)
        console.log('un contrato')
        
    } catch (e) {
        res.status(401).send('user no valido')
        console.log('error database')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}




module.exports = {
    createContrato,
    confirmationContrat,
    selectContratos,
    selectOneContrat
}