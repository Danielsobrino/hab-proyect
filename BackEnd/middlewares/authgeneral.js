require('dotenv').config();
const jwt = require('jsonwebtoken');


const isLogin = (req, res, next) => {
    // obtengo el token que habran metido en las cabeceras
    // cortar cabecera .substr(7)(FRONTEND)
    const { authorization } = req.headers;
    try {
        const decodedToken = jwt.verify(authorization.substr(7), process.env.SECRET);
        req.auth = decodedToken;

        // VERIFICAMOS QUE ES UNA FAMILIA
        if (req.auth.role === 'familia') {
            console.log('login familia')
           next()
        }else if (req.auth.role === 'ojeador'){
            console.log('login ojeador')
            next()
        }else {
            throw new Error('error')
        }
    } catch (error) {
        console.log('Wops!', error)
        next(error)
    }   
}



module.exports = {
    isLogin,
}