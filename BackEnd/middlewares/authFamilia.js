require('dotenv').config();
const jwt = require('jsonwebtoken');


const isLoginFamily = (req, res, next) => {
    // obtengo el token que habran metido en las cabeceras
    // cortar cabecera .substr(7)(FRONTEND)
    const { authorization } = req.headers;
    try {
        const decodedToken = jwt.verify(authorization.substr(7), process.env.SECRET);
        req.auth = decodedToken;

        // VERIFICAMOS QUE ES UNA FAMILIA
        if (req.auth.role !== 'familia') {
            throw new Error('error')
        }
    } catch (error) {
        console.log('Wops!', error)
        next(error)
    }
    console.log('ERES UNA FAMILIA')
    next();
}


module.exports = {
    isLoginFamily
}




