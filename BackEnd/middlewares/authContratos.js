require ('dotenv').config();
const jwt = require('jsonwebtoken');
const { getConnection } = require("../db/db");

async function familyForContrat(req, res, next) {

    const email = req.auth.email;
    const { code } = req.params;
    let connection;

    try {
        connection = await getConnection();
        const [emailFamilia] = await connection.query(`
        SELECT email FROM jugador_ojeador WHERE code = ? `, [code])
        console.log(emailFamilia[0].email)
        const correoFamilia = emailFamilia[0].email
        
        if (email === correoFamilia) {
            console.log('si')
            next()
        } else {
            console.log('NO ES EL MISMO USUARIO')
            res.status(500).send('NO ES EL MISMO USUARIO')
            return
        }
    } catch (e) {
        res.status(401).send()
        console.log('ERROR NO ERES EL ADM DE ESTE USUARIO')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}

module.exports = {
    familyForContrat
}