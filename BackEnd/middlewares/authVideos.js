const { getConnection } = require("../db/db");



//COMPROBAMOS QUE ES LA MISMA FAMILIA QUE LA QUE CREO QUE JUGADOR.
async function familyForVideo(req, res, next) {

    const { id } = req.auth;
    const { idvideo } = req.params;
    let connection;

    try {
        connection = await getConnection();
        const [idfamilia] = await connection.query(`
        SELECT id_familia FROM videos WHERE id = ? `, [idvideo])
    

        if (id === idfamilia[0].id_familia) {
            next()
        } else {
            console.log('NO ES EL MISMO USUARIO')
            res.status(500).send('NO ES EL MISMO USUARIO')
            return
        }
    } catch (e) {
        res.status(401).send()
        console.log('ERROR NO ERES EL ADM DE ESTE USUARIO')
        console.log(e)

    } finally {
        if (connection) {
            connection.release()
        }
    }
}

module.exports = {
    familyForVideo
}