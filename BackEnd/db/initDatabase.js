require('dotenv').config()

const { getConnection } = require("./db");


async function main() {

  let connection;

  try {
    connection = await getConnection();

    
    
    await connection.query ('DROP TABLE IF EXISTS videos');
    await connection.query ('DROP TABLE IF EXISTS jugador_ojeador');
    await connection.query ('DROP TABLE IF EXISTS jugadores');
    await connection.query ('DROP TABLE IF EXISTS ojeadores');
    await connection.query ('DROP TABLE IF EXISTS familia');



    await connection.query(` 
        CREATE TABLE familia (
            id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
            fecha_creacion DATE ,
            fecha_modificacion DATE ,
            nombre VARCHAR(50) NOT NULL,
            apellido_1 VARCHAR(50) NOT NULL,
            apellido_2 VARCHAR(50) NOT NULL,
            provincia VARCHAR(50) NOT NULL,
            ciudad VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL UNIQUE,
            avatar VARCHAR(200),
            contrasena VARCHAR(200) NOT NULL,
            telefono VARCHAR(50) NOT NULL UNIQUE,
            validationCode VARCHAR(200) NOT NULL,
            recoveryCode VARCHAR (10) ,
            active BOOLEAN default false
        );`)
        console.log('tabla familia creada')


    await connection.query(` 
        CREATE TABLE jugadores (
            id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
            dni VARCHAR(9) NOT NULL,
            email VARCHAR(50) NOT NULL,
            nombre VARCHAR(50) NOT NULL,
            apellido_1 VARCHAR(50) NOT NULL,
            apellido_2 VARCHAR(50) NOT NULL,
            descripcion VARCHAR(200) NOT NULL,
            edad VARCHAR(10) NOT NULL,
            sexo ENUM('masculino', 'femenino'),
            provincia VARCHAR(50) NOT NULL,
            categoria VARCHAR(50) NOT NULL,
            club VARCHAR(50) NOT NULL,
            posicion_principal VARCHAR(50) NOT NULL,
            pierna ENUM ('derecha' , 'izquierda') NOT NULL,
            altura VARCHAR(50),
            peso VARCHAR(50),
            avatar VARCHAR(200),
            id_familia INTEGER UNSIGNED NOT NULL,
            CONSTRAINT jugadores_idfamilia FOREIGN KEY (id_familia) REFERENCES familia(id) on delete cascade
        );`)
        
        console.log('tabla jugadores creada')


    await connection.query(` 
        CREATE TABLE ojeadores (
            id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
            fecha_creacion DATE NOT NULL,
            fecha_modificacion DATE NOT NULL,
            nombre VARCHAR(50) NOT NULL,
            apellido_1 VARCHAR(50) NOT NULL,
            apellido_2 VARCHAR(50) NOT NULL,
            descripcion VARCHAR(200) NOT NULL,
            provincia VARCHAR(50) NOT NULL,
            club VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL UNIQUE,
            avatar VARCHAR(200),
            contrasena VARCHAR(200) NOT NULL,
            telefono VARCHAR(50) NOT NULL UNIQUE,
            validationCode VARCHAR(200) NOT NULL ,
            recoveryCode VARCHAR (10) ,
            active BOOLEAN default false
        );`)
        console.log('tabla ojeadores creada')

    await connection.query(` 
        CREATE TABLE videos (
            id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
            fecha_creacion DATE NOT NULL,
            titulo VARCHAR(150) NOT NULL,
            mensaje TEXT NOT NULL,
            url_video VARCHAR(200),
            id_jugador INTEGER UNSIGNED NOT NULL,
            CONSTRAINT videos_idjugador FOREIGN KEY (id_jugador) REFERENCES jugadores(id) on delete cascade,
            id_familia INTEGER UNSIGNED NOT NULL,
            CONSTRAINT videos_idfamilia  FOREIGN KEY (id_familia) REFERENCES familia(id) on delete cascade
            
        );`)
          
        console.log('tabla videos creada')


    await connection.query(` 
        CREATE TABLE jugador_ojeador (
            id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
            titulo VARCHAR(50) NOT NULL,
            estado ENUM ('vigor' , 'espera' , 'rechazado') default 'espera',
            mensaje TEXT NOT NULL,
            email VARCHAR(50) NOT NULL,
            code VARCHAR(50) NOT NULL,
            id_jugador INTEGER UNSIGNED NOT NULL,
            CONSTRAINT jugador_ojeador_idjugador FOREIGN KEY (id_jugador) REFERENCES jugadores (id),
            id_ojeador INTEGER UNSIGNED NOT NULL,
            CONSTRAINT jugador_ojeador_idojeador FOREIGN KEY (id_ojeador) REFERENCES ojeadores (id)
        );`)
      

        console.log('tabla contratos creada')
  } catch (e) {
    console.log('algo va mal', e)
  } finally {
    if(connection){
      connection.release();
    }
    process.exit();
  }}

  (async () => {
    await main()
  })()

